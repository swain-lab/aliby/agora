# -*- coding: utf-8 -*-
"""
logfile_parser
~~~~~~~~~~~~

Simple log file parsing according to grammars specified in JSON

:copyright: (c) 2020 by Julian Pietsch.
:license: LGPL
"""

from .logfile_parser import Parser
