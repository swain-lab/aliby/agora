# Agora
Shared tools for data processing within the aliby pipeline.

## Installation

If you just want to use the tools.

```bash
> pip install aliby-argo
```

Or, for development, clone this repository and then install using pip:

```bash
> pip install -e argo/
```
