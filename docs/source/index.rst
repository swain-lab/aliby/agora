.. aliby documentation master file, created by
   sphinx-quickstart on Thu May 19 12:18:46 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to agora's documentation!
=================================

.. autosummary::
   :toctree: _autosummary
   :template: custom-module-template.rst
   :recursive:

   agora
